const express = require("express");
const mongoose = require("mongoose");
const Profsante = require("./models/profsantemodel");
const bodyParser = require('body-parser');
const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/profsante", async (req, res) => {
    try {
        await profsante.find({})
        .then(result=>{
            res.send(result)
        });
        }
    catch (err) {
        console.log(err);
    }
});

app.post("/ajouter_profsante", async (req, res) => {
   try{
     let new_profsante = new Profsante({
       titre: req.body.titre,
       metier: req.body.metier,
       nom: req.body.nom,
       prenom: req.body.prenom,
       email: req.body.email,
       numidentite: req.body.num_identite,
       matricule_employé: req.body.matricule_employé,
       adresse: req.body.adresse,
       mdp: req.body.mdp,
       RPPS: req.body.rpps,
       Fax: req.body.fax,
       Telephone: req.body.telephone
     });
       await new_profsante.save();
        res.send("save effectué avec succes!");
    } catch (err) {
        console.log(err);
    }
});

mongoose.connect('mongodb+srv://fatmazg:fatmazg@cluster0.wuzkfd1.mongodb.net/database?retryWrites=true&w=majority&appName=Cluster0', {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then(() => {
    console.log('Connexion à la base de données MongoDB réussie');
}).catch((err) => {
    console.error('Erreur de connexion à la base de données MongoDB :', err);
});



app.listen(5000,()=> console.log("serveur en marche"));
