const mongoose = require('mongoose')

const profsanteSchema = mongoose.Schema({
    titre: {
        type: String,
        required: true,
    },
    metier: {
        type: String,
        required: true,
    },
    nom: {
        type: String,
        required: true,
    },
    prenom: {
        type: String,
        required: true,
    },
    adresse: {
        type: String,
        required: true,
    },
    mdp: {
        type: String,
        required: true,
    },
    numidentite: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    Telephone: {
        type: String,
        required: true,
    },
    RPPS: {
        type: String,
        required: true,
    },    
    Fax: {
        type: String,
        required:false ,
    }
    
    
    
    // Autres champs spécifiques au professionnel de santé...
});


module.exports = Profsante = mongoose.model('profsantes', profsanteSchema);
