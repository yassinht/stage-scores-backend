const mongoose = require('mongoose')

const patientSchema = mongoose.Schema({
    nom: {
        type: String,
        required: true,
    },
    prenom: {
        type: String,
        required: true,
    },
    adresse: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    Telephone: {
        type: String,
        required: true,
    },
    dateNaissance: {
        type: Date,
        required: true,
    },
    poids_kg: {
        type: Number,
        required: true,
    },
    taille_cm: {
        type: Number,
        required: true,
    },
    mdp: {
        type: String,
        required: true,
    },
    num_sécurité_social: {
        type: String,
        required: false,
    }
    // Autres champs spécifiques au patient...
});


module.exports = Patient = mongoose.model('patient', patientSchema);