const mongoose = require('mongoose')

const superadminSchema = mongoose.Schema({
    nom: {
        type: String,
        required: true,
    },
    prenom: {
        type: String,
        required: true,
    },
    num_identite: {
        type: String,
        required: true,
    },
    matricule_employé: {
        type: String,
        required: true,
    },
    adresse: {
        type: String,
        required: true,
    },
    mdp: {
        type: String, 
        required: true,
    },
    email: {
        type: String,
        required: true,
    }
});

module.exports = Superadmin = mongoose.model('superadmin', superadminSchema);
